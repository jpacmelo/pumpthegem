// Project description
After 3 weeks of learning Java, we were challenged to create a game. The first group project at <Academia de Código_> #62 Bootcamp @ Lisboa, took part during the 4th week and the main goal was to apply the technologies we have learned so far.
Deadline for deployment: 10 nights!

//The Idea
After our brainstorm, we decide to create a game that would mirror the creative and logical process that a <Code_Cadet> goes through to overcome a challenge. Every developer knows that by externalizing his reasoning, it becomes more clear and new ideas come to mind. In this multi-level game, the goal is to bring the rubber duck back to the computer, in order to overcome the code challenge.

//Development
The first thing we did was to decide the minimum viable product for our application and then we structured the project with a design of the UML schema. Mainly working in pair programming, we developed features one by one (player movement, gravity for game objects, player interaction with game objects, enemies, level goal, next level, audio effects, graphic elements and user interface).

//Tech && Methodologies
Java 7, OOP, SimpleGFX Library, IntelliJ IDEA, Apache Ant, Git, Paint 3D
