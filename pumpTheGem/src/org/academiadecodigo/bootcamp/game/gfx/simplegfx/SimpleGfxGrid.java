package org.academiadecodigo.bootcamp.game.gfx.simplegfx;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class SimpleGfxGrid implements Grid {

    public static final int PADDING = 10;
    private int cols;
    private int rows;
    private int cellSize = 40;
    private int width;
    private int height;
    private Rectangle field;
    private Picture picture;

    public SimpleGfxGrid(int cols, int rows){
        this.rows = rows;
        this.cols = cols;

    }

    /**
     * @see Grid#init()
     */
    @Override
    public void init() {
        width=cols*cellSize;
        height=rows*cellSize;
        //field = new Rectangle(PADDING, PADDING, width, height);
        //field.draw();
        picture = new Picture(PADDING,PADDING,"bg.png");
        picture.draw();

    }

    /**
     * @see Grid#getCols()
     */
    @Override
    public int getCols() {
        return cols;
    }

    /**
     * @see Grid#getRows()
     */
    @Override
    public int getRows() {
        return rows;
    }

    /**
     * Obtains the width of the grid in pixels
     * @return the width of the grid
     */
    public int getWidth() {
        return width;
    }

    /**
     * Obtains the height of the grid in pixels
     * @return the height of the grid
     */
    public int getHeight() {
        return height;
    }

    /**
     * Obtains the grid X position in the SimpleGFX canvas
     * @return the x position of the grid
     */
    public int getX() {
        int x=0;
        if(field != null) {
            x= field.getX();
        }
        if(picture != null){
            x= picture.getX();
        }
        return x;
    }

    /**
     * Obtains the grid Y position in the SimpleGFX canvas
     * @return the y position of the grid
     */
    public int getY() {
        int y=0;
        if(field != null) {
            y= field.getY();
        }
        if(picture != null){
            y= picture.getY();
        }
        return y;
    }

    /**
     * Obtains the pixel width and height of a grid position
     * @return
     */
    public int getCellSize() {
        return cellSize;
    }

    /**
     * @see Grid#makeGridPosition()
     */
    /*@Override
    public GridPosition makeGridPosition() {
        GridPosition grid;
        grid = new SimpleGfxGridPosition(this);
        return grid;
    }*/

    /**
     * @see Grid#makeGridPosition(int, int)
     */
    @Override
    public GridPosition makeGridPosition(int col, int row) {
        GridPosition pos;
        pos = new SimpleGfxGridPosition(col, row, this);
        return pos;
    }

    /**
     * Auxiliary method to compute the y value that corresponds to a specific row
     * @param row index
     * @return y pixel value
     */
    public int rowToY(int row) {
        return (row)*cellSize + PADDING;
    }

    /**
     * Auxiliary method to compute the x value that corresponds to a specific column
     * @param column index
     * @return x pixel value
     */
    public int columnToX(int column) {
        return (column)*cellSize + PADDING;
    }

    public void hide(){
        if(field != null){
            field.delete();
        }
        if(picture != null){
            picture.delete();
        }

    }

    public void setField(int cols, int rows) {
        this.field = new Rectangle(PADDING, PADDING, cols, rows);
    }

}
