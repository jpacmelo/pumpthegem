package org.academiadecodigo.bootcamp.game.gfx.simplegfx;

import org.academiadecodigo.bootcamp.game.grid.GridColor;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;
import org.academiadecodigo.bootcamp.game.grid.position.AbstractGridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;
    private Picture picture;

    /**
     * Simple graphics position constructor
     * @param grid Simple graphics grid
     */
    /*public SimpleGfxGridPosition(SimpleGfxGrid grid){
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);
        simpleGfxGrid = grid;
        rectangle = new Rectangle(grid.columnToX(getCol()),grid.rowToY(getRow()), grid.getCellSize(), grid.getCellSize());
        rectangle.fill();

    }*/

    /**
     * Simple graphics position constructor
     * @param col position column
     * @param row position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid){
        super(col, row, grid);
        simpleGfxGrid = grid;
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        if(rectangle != null){
            rectangle.fill();
        }
        if(picture != null){
            picture.draw();
        }
    }


    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        if(rectangle != null) {
            rectangle.delete();
        }
        if(picture != null){
            picture.delete();
        }
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {
        switch (direction) {

            case UP:
                int maxRowsUp = distance < getRow() ? distance : getRow();
                super.setPos(getCol(), getRow() - maxRowsUp);
                if(rectangle != null) {
                    rectangle.translate(0, -(maxRowsUp * simpleGfxGrid.getCellSize()));
                }
                if(picture != null) {
                    picture.translate(0, -(maxRowsUp * simpleGfxGrid.getCellSize()));
                }
                break;
            case DOWN:
                int maxRowsDown = distance > simpleGfxGrid.getRows() - (getRow() + 1) ? simpleGfxGrid.getRows() - (getRow() + 1) : distance;
                super.setPos(getCol(), getRow() + maxRowsDown);
                if(rectangle != null) {
                    rectangle.translate(0, (maxRowsDown * simpleGfxGrid.getCellSize()));
                }
                if(picture != null) {
                    picture.translate(0, (maxRowsDown * simpleGfxGrid.getCellSize()));
                }
                break;
            case LEFT:
                int maxRowsLeft = distance < getCol() ? distance : getCol();
                super.setPos(getCol() - maxRowsLeft, getRow());
                if(rectangle != null) {
                    rectangle.translate(-(maxRowsLeft * simpleGfxGrid.getCellSize()), 0);
                }
                if(picture != null) {
                    picture.translate(-(maxRowsLeft * simpleGfxGrid.getCellSize()), 0);
                }
                break;
            case RIGHT:
                int maxRowsRight = distance > getGrid().getCols() - (getCol() + 1) ? getGrid().getCols() - (getCol() + 1) : distance;
                super.setPos(getCol() + maxRowsRight, getRow());
                if(rectangle != null) {
                    rectangle.translate((maxRowsRight * simpleGfxGrid.getCellSize()), 0);
                }
                if(picture != null) {
                    picture.translate((maxRowsRight * simpleGfxGrid.getCellSize()), 0);
                }
                break;
        }
    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        rectangle = new Rectangle(simpleGfxGrid.columnToX(getCol()),simpleGfxGrid.rowToY(getRow()),simpleGfxGrid.getCellSize(),simpleGfxGrid.getCellSize());
        super.setColor(color);
        rectangle.setColor(SimpleGfxColorMapper.getColor(color));
        show();
    }

    @Override
    public void setImg(String imgName) {
        picture = new Picture(simpleGfxGrid.columnToX(getCol()),simpleGfxGrid.rowToY(getRow()),imgName);
        super.setImg(imgName);
        picture.draw();
    }

    public Picture getPicture(){
        return picture;
    }
}
