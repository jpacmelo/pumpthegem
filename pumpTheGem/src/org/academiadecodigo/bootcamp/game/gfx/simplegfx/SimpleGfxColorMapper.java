package org.academiadecodigo.bootcamp.game.gfx.simplegfx;


import org.academiadecodigo.bootcamp.game.grid.GridColor;
import org.academiadecodigo.simplegraphics.graphics.Color;



public class SimpleGfxColorMapper {

    private static final Color[] colors = {
            Color.RED,
            Color.GREEN,
            Color.BLUE,
            Color.MAGENTA,
            Color.WHITE,
            Color.CYAN,
            Color.DARK_GRAY,
            Color.GRAY,
            Color.LIGHT_GRAY,
            Color.BLACK,
            Color.ORANGE,
            Color.PINK,
            Color.YELLOW
    };

    public static Color getColor(GridColor color){

        Color sGfxColor = null;

        switch (color) {
            case RED:
                sGfxColor = colors[0];
                break;
            case GREEN:
                sGfxColor = colors[1];
                break;
            case BLUE:
                sGfxColor = colors[2];
                break;
            case MAGENTA:
                sGfxColor = colors[3];
                break;
            case NOCOLOR:
                sGfxColor = colors[4];
                break;
            case CYAN:
                sGfxColor = colors[5];
                break;
            case DARK_GRAY:
                sGfxColor = colors[6];
                break;
            case GRAY:
                sGfxColor = colors[7];
                break;
            case LIGHT_GRAY:
                sGfxColor = colors[8];
                break;
            case BLACK:
                sGfxColor = colors[9];
                break;
            case ORANGE:
                sGfxColor = colors[10];
                break;
            case PINK:
                sGfxColor = colors[11];
                break;
            case YELLOW:
                sGfxColor = colors[12];
                break;
            default:
                System.out.println("something went terribly wrong...");
        }

        return sGfxColor;
    }
}
