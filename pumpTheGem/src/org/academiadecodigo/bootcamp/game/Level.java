package org.academiadecodigo.bootcamp.game;

import org.academiadecodigo.bootcamp.game.controller.Controller;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;

public class Level {

    private GameObject[] gameObjs;
    private Controller controller;

    public Level(GameObject[] gameObjs, Controller controller){
        this.gameObjs = gameObjs;
        this.controller = controller;

    }

    public void init(){
        controller.getPlayer().getPos().show();
        controller.setObjectsToInteract(gameObjs); //Pedir ao João para me explicar melhor isto!
        controller.activate();

    }




}
