package org.academiadecodigo.bootcamp.game;

import org.academiadecodigo.bootcamp.game.Features.Sound;
import org.academiadecodigo.bootcamp.game.controller.Controller;
import org.academiadecodigo.bootcamp.game.gameObject.Destroyable;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.gameObject.characters.Beetle;
import org.academiadecodigo.bootcamp.game.gameObject.characters.Enemy;
import org.academiadecodigo.bootcamp.game.gameObject.characters.EnemyFactory;
import org.academiadecodigo.bootcamp.game.gameObject.props.Gem;
import org.academiadecodigo.bootcamp.game.gameObject.props.Prop;
import org.academiadecodigo.bootcamp.game.gameObject.props.PropFactory;
import org.academiadecodigo.bootcamp.game.gameObject.terrain.Portal;
import org.academiadecodigo.bootcamp.game.gameObject.terrain.Terrain;
import org.academiadecodigo.bootcamp.game.gameObject.terrain.TerrainFactory;
import org.academiadecodigo.bootcamp.game.gfx.simplegfx.SimpleGfxGrid;
import org.academiadecodigo.bootcamp.game.gfx.simplegfx.SimpleGfxGridPosition;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.simplegraphics.graphics.Canvas;
import org.academiadecodigo.simplegraphics.graphics.Shape;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.ArrayList;


public class Game {

    private int delay;
    private Grid grid;
    int lowestCell;
    private int gemCounter = 0;
    private Portal portal;
    private GameObject[] gameObjs;
    private Controller controller;
    private int totalGems;
    private Gem[] gems;
    private Level level;
    private boolean isHolding = true;
    private boolean reset = false;
    private Beetle[] enemies;


    public Game(Grid grid, int delay) {
        this.grid = grid;
        this.delay = delay;
        this.lowestCell = this.grid.getRows() - 1;
    }

    public void menu() throws InterruptedException {

        controller = new Controller(this, grid, grid.makeGridPosition(16, 1));
        controller.init();

        Picture menu = new Picture(10, 10, "gamefirstmenu.jpg");
        menu.draw();
        Sound soundFirst = new Sound("firstsong.wav");
        soundFirst.play(true);
        soundFirst.setLoop(5);
        //soundFirst.stop();
        while (isHolding) {
            Thread.yield();
        }

        menu.delete();
        hold();
        Picture instructions = new Picture(10, 10, "gameinstructions.jpg");
        instructions.draw();
        while (isHolding) {
            Thread.yield();
        }

        soundFirst.stop();
        init();

        start();
    }

    public void init() {
        grid.init();
        //controller = new Controller(this, grid, grid.makeGridPosition(16, 1));
        //controller.init();
    }

    public void start() throws InterruptedException {

        countAndBuildGameObjectsLvl1(); //Pedir ao João para me esclarecer em como está agora a fazer a contagem...
        level = new Level(gameObjs, controller);
        level.init();
        Sound sound = new Sound("DuckSound.wav");
        sound.play(true);
        sound.setLoop(10);
        //sound.stop();//Put this in comment to stop the music while we are working
        gameBehavior();
        controller.deactivate();

        sound.stop();
        Sound soundLevelPassed = new Sound("levelwinner.wav");
        soundLevelPassed.play(true);
        Picture betweenLevels1 = new Picture(10, 10, "gamenextlevel.jpg");
        betweenLevels1.draw();
        while (isHolding) {
            Thread.yield();
        }

        grid.init();
        countAndBuildGameObjectsLvl2();
        level = new Level(gameObjs, controller);
        level.init();
        soundLevelPassed.stop();
        sound.play(true);
        gameBehavior();

        sound.stop();
        soundLevelPassed.play(true);
        Picture betweenLevels2 = new Picture(10, 10, "gamenextlevel.jpg");
        betweenLevels2.draw();
        while (isHolding) {
            Thread.yield();
        }

        grid.init();
        countAndBuildGameObjectsLvl3();
        level = new Level(gameObjs, controller);
        level.init();
        soundLevelPassed.stop();
        sound.play(true);
        gameBehavior();

        sound.stop();
        soundLevelPassed.play(true);
        Picture betweenLevels3 = new Picture(10, 10, "gamenextlevel.jpg");
        betweenLevels3.draw();
        while (isHolding) {
            Thread.yield();
        }

        grid.init();
        countAndBuildGameObjectsLvl4();
        level = new Level(gameObjs, controller);
        level.init();
        soundLevelPassed.stop();
        sound.play(true);
        gameBehavior();

        sound.stop();
        soundLevelPassed.play(true);
        Picture betweenLevels4 = new Picture(10, 10, "gamenextlevel.jpg");
        betweenLevels4.draw();
        while (isHolding) {
            Thread.yield();
        }

        grid.init();
        countAndBuildGameObjectsLvl5();
        level = new Level(gameObjs, controller);
        level.init();
        soundLevelPassed.stop();
        sound.play(true);
        gameBehavior();

        sound.stop();
        soundLevelPassed.play(true);
        Picture betweenLevels5 = new Picture(10, 10, "gamenextlevel.jpg");
        betweenLevels5.draw();
        while (isHolding) {
            Thread.yield();
        }

        grid.init();
        countAndBuildGameObjectsLvl6();
        level = new Level(gameObjs, controller);
        level.init();
        soundLevelPassed.stop();
        sound.play(true);
        gameBehavior();

        sound.stop();
        soundLevelPassed.play(true);
        Picture betweenLevels6 = new Picture(10, 10, "gamenextlevel.jpg");
        betweenLevels6.draw();
        while (isHolding) {
            Thread.yield();
        }

        grid.init();
        countAndBuildGameObjectsLvl7();
        level = new Level(gameObjs, controller);
        level.init();
        soundLevelPassed.stop();
        sound.play(true);
        gameBehavior();

        sound.stop();
        soundLevelPassed.play(true);
        Picture endGame = new Picture(10, 10, "endgame.jpg");
        endGame.draw();
        while (isHolding) {
            Thread.yield();
        }
        hold();
        soundLevelPassed.stop();
        sound.stop();
    }

    // apply gravity to props
    public void gravity() {
        // for each existing prop apply gravity
        for (GameObject gameObj : gameObjs) {
            if (gameObj instanceof Prop) {
                Prop prop = (Prop) gameObj;
                // Get current row
                int curRow = prop.getPos().getRow();
                int curCol = prop.getPos().getCol();

                boolean isEmpty = true;
                // For each terrain, check if the position matches with the position under my prop
                for (GameObject otherGameObj : gameObjs) {
                    if (otherGameObj instanceof Terrain) {
                        Terrain terrain = (Terrain) otherGameObj;
                        // Compare terrain position with position under the prop
                        if (terrain.getPos().getRow() == curRow + 1 && terrain.getPos().getCol() == curCol) {
                            // if match compare the terrain type to Destroyable
                            if (terrain instanceof Destroyable) {
                                //if match check if exists
                                Destroyable destroyableTerrain = (Destroyable) terrain;
                                if (!destroyableTerrain.exists()) {
                                    //if it does, cell is empty
                                    break;
                                }
                            }
                            // If is not of type destroyable, no check necessary, just assume cell is not empty
                            isEmpty = false;
                            break;
                        }
                    }
                }
                if (isEmpty) {
                    // For each prop, check if the position matches with the position under my prop
                    for (GameObject otherGameObj : gameObjs) {
                        if (otherGameObj instanceof Prop) {
                            Prop otherProp = (Prop) otherGameObj;
                            if (otherProp != prop) {
                                if (otherGameObj.getPos().getRow() == curRow + 1 && otherGameObj.getPos().getCol() == curCol && otherProp.exists()) {
                                    isEmpty = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (isEmpty) {
                    //Props can collide with player
                    if (controller.getPos().getRow() == curRow + 1 && controller.getPos().getCol() == curCol) {
                        isEmpty = false;
                    }
                }
                // If position is empty, trigger, move down.
                if (isEmpty) {
                    prop.move(GridDirection.DOWN);
                }
            }
        }
    }

    public void portalBehavior() {
        if (portal.isOpen()) {
            portal.getTeleportable(controller.getPlayer());
        } else {
            for (Gem gema : gems) {
                if (portal.getTeleportable(gema)) {
                    gemCounter++;
                    if (gemCounter == totalGems) {
                        portal.open();
                    }
                }
            }
        }
    }

    public void countAndBuildGameObjectsLvl1() {

        gameObjs = null;
        enemies = null;

        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        ArrayList<GameObject> lvlObjs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();


        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        //A6
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            /*if (i == grid.getCols() - 2) {
                portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(i, lowestCell));
                lvl1Objs.add(portal);
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }*/
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }
        for (int i = 0; i < grid.getRows(); i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }
        for (int i = 6; i < 9; i++) {
            for (int j = 3; j < 9; j++) {
                lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(j, i)));
            }
        }
        for (int i = 6; i < 9; i++) {
            for (int j = 1; j < 3; j++) {
                lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(j, i)));
            }
        }

        for (int i = 2; i < grid.getCols() - 1; i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 10)));
        }
        //construction of the arrows in grass
        for (int i = 1; i < grid.getRows() - 6; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(12, i)));
        }
        for (int i = 5; i < grid.getRows() - 2; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(14, i)));
        }
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(11, 2)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(11, 3)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(11, 4)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(10, 3)));
        for (int i = 13; i < grid.getCols() - 2; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 3)));
        }
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(16, 7)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 6)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 7)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 8)));
        for (int i = 11; i < grid.getCols() - 4; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 7)));
        }

        //AC LOGO
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(2, 3)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(3, 2)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(4, 1)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(5, 2)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(6, 3)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(7, 2)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(7, 4)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 1)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 5)));

        //wall where player starts
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(16, 2)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(15, 2)));


        portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(1, 10));
        lvlObjs.add(portal);

        Gem gem = PropFactory.createGem(grid, grid.makeGridPosition(6, 2));
        lvlObjs.add(gem);
        lvlGems.add(gem);
        totalGems++;

        /*ArrayList<Enemy> lvlEnemies = new ArrayList<>();

        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(1, 1), GridDirection.DOWN, controller.getPlayer());
        lvlObjs.add(a);
        lvlEnemies.add(a);*/

        gameObjs = new GameObject[lvlObjs.size()];
        lvlObjs.toArray(gameObjs);

        gems = new Gem[lvlGems.size()];
        lvlGems.toArray(gems);

        /*enemies = new Beetle[lvlEnemies.size()];
        lvlEnemies.toArray(enemies);

        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.LEFT);
        a = null;*/

    }

    public void countAndBuildGameObjectsLvl2() {

        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        gameObjs = null;
        enemies = null;

        ArrayList<GameObject> lvl1Objs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();

        //A1
        for (int i = 2; i < grid.getCols() - 9; i++) {
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 2)));
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 4)));
        }
        //A2
        for (int i = 2; i < grid.getCols() - 9; i++) {
            if (i == grid.getCols() - 13) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 3)));
        }

        //Cycle to build side walls
        //A3
        for (int i = 0; i < grid.getRows(); i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }

        //Cycle to build middle walls between player and sand
        //A4
        for (int i = 1; i < grid.getRows() - 5; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(10, i)));
        }

        //Cycle to build walls under the block of sand with two small opening for the gem
        //A5
        for (int i = 2; i < grid.getCols() - 9; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 6)));
        }

        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        //A6
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            if (i == grid.getCols() - 2) {
                portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(i, lowestCell));
                lvl1Objs.add(portal);
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }

        //Cycle to built maze walls at the beginning;
        //A7
        for (int i = 11; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 6) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 2)));
        }
        //A8
        for (int i = 11; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 3) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 4)));
        }
        //A9
        for (int i = 11; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 5) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 6)));
        }

        //built a pyramid of obstacles; rock and sand
        //Rock
        //A10
        for (int i = 8; i < grid.getCols() - 5; i++) {
            lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 10)));
        }
        //A11
        for (int i = 9; i < grid.getCols() - 6; i++) {
            lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 9)));
        }
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(10, 8)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(10, 7)));
        //Sand
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(9, 8)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(11, 8)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 9)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(12, 9)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(7, 10)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(13, 10)));


        //Build props, gems

        Gem gem1 = PropFactory.createGem(grid, grid.makeGridPosition(5, 3));
        lvl1Objs.add(gem1);
        lvlGems.add(gem1);
        totalGems++;

        ArrayList<Enemy> lvlEnemies = new ArrayList<>();
        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(1, 1), GridDirection.DOWN, controller.getPlayer());
        lvl1Objs.add(a);
        lvlEnemies.add(a);

        gameObjs = new GameObject[lvl1Objs.size()];
        gems = new Gem[lvlGems.size()];
        enemies = new Beetle[lvlEnemies.size()];

        lvl1Objs.toArray(gameObjs);
        lvlGems.toArray(gems);
        lvlEnemies.toArray(enemies);







        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.LEFT);
        a = null;

    }

    public void countAndBuildGameObjectsLvl3() {

        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        gameObjs = null;
        enemies = null;

        ArrayList<GameObject> lvl1Objs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();

        //Cycle to build side walls
        for (int i = 0; i < grid.getRows(); i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }
        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            if (i == grid.getCols() - 2) {
                portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(i, lowestCell));
                lvl1Objs.add(portal);
                continue;
            }
            if (i == grid.getCols() - 3) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }

        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 16) {
                continue;
            }
            if (i == grid.getCols() - 3) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 4)));
        }

        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(8, 3)));

        for (int i = 5; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 11) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 5) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 2)));
        }

        // row one
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(6, 1)));
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(12, 1)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(3, 1)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(8, 1)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(11, 1)));

        for (int i = 1; i < grid.getCols() - 2; i++) {
            if (i == grid.getCols() - 15) {
                continue;
            }
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 1)));
        }

        //row two
        for (int i = 1; i < grid.getCols() - 4; i++) {
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 13) {
                continue;
            }
            if (i == grid.getCols() - 15) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 2)));
        }

        //row three
        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 3)));
        }

        //row four
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(2, 4)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 4)));

        //row five
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(1, 5)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(12, 5)));

        //row six
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(1, 6)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(8, 6)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(12, 6)));

        //row seven
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(1, 7)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(3, 7)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(4, 7)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(8, 7)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(12, 7)));

        // row eight
        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 14) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 3) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 8)));
        }
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(4, 8)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 8)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(12, 8)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 8)));

        //row nine
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 9)));
        }

        //row ten
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 10)));
        }

        //Build props, gems
        Gem gem1 = PropFactory.createGem(grid, grid.makeGridPosition(3, 2));
        lvl1Objs.add(gem1);
        lvlGems.add(gem1);
        totalGems++;

        Gem gem2 = PropFactory.createGem(grid, grid.makeGridPosition(11, 3));
        lvl1Objs.add(gem2);
        lvlGems.add(gem2);
        totalGems++;

        portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(16, 11));
        lvl1Objs.add(portal);





        ArrayList<Enemy> lvlEnemies = new ArrayList<>();

        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(2, 5), GridDirection.RIGHT, controller.getPlayer());
        lvl1Objs.add(a);
        lvlEnemies.add(a);

        enemies = new Beetle[lvlEnemies.size()];
        lvlEnemies.toArray(enemies);



        gameObjs = new GameObject[lvl1Objs.size()];
        gems = new Gem[lvlGems.size()];
        lvl1Objs.toArray(gameObjs);
        lvlGems.toArray(gems);
        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.UP);
        a = null;

    }

    public void countAndBuildGameObjectsLvl4() {

        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        gameObjs = null;
        enemies = null;

        ArrayList<GameObject> lvlObjs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();

        //controller.getPlayer().getPos().setPos(1,16);

        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        //A6
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }
        for (int i = 0; i < grid.getRows(); i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }
        //Wall inverted triangle frame at the middle
        //Left side
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(1, 1)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(2, 2)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(3, 3)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(4, 4)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(5, 5)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(6, 6)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(7, 7)));
        //Right side
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(15, 1)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(14, 2)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(13, 3)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(12, 4)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(11, 5)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(10, 6)));
        lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(9, 7)));


        //Grass under the duck
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(1, 3)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(2, 3)));

        //Grass inside the triangle
        for (int i = 2; i < grid.getCols() - 3; i++) {
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 1)));
        }
        for (int i = 3; i < grid.getCols() - 4; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 2)));
        }
        for (int i = 4; i < grid.getCols() - 5; i++) {
            if (i == grid.getCols() - 7) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 3)));
        }
        for (int i = 5; i < grid.getCols() - 6; i++) {
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 4)));
        }
        for (int i = 6; i < grid.getCols() - 7; i++) {
            if (i == grid.getCols() - 10) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 5)));
        }
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 6)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(9, 6)));

        //fill the triangle with rocks
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(9, 1)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(11, 1)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(11, 3)));
        for (int i = 8; i < grid.getCols() - 7; i++) {
            lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 4)));
        }
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(8, 5)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(7, 6)));

        //Grass going out of the triangle
        for (int i = 7; i < grid.getRows() - 1; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, i)));
        }

        //Grass out of the triangle
        for (int i = 1; i < grid.getCols() - 10; i++) {
            if (i == grid.getCols() - 13) {
                continue;
            }
            if (i == grid.getCols() - 14) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 9)));
        }
        for (int i = 8; i < grid.getCols() - 7; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 10)));
        }

        //Wall close to the portal
        for (int i = 2; i < grid.getCols() - 12; i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 10)));
        }

        //hall for the player on the right side with grass and rocks
        //Rocks
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(16, 2)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(15, 2)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(14, 3)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(13, 4)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(12, 5)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(11, 6)));
        lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(10, 7)));
        //Grass
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(16, 5)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 5)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(14, 6)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(13, 7)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(12, 8)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(11, 9)));
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(10, 10)));

        for (int i = 1; i < 6; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 6)));
        }

        Gem gem1 = PropFactory.createGem(grid, grid.makeGridPosition(1, 2));
        lvlObjs.add(gem1);
        lvlGems.add(gem1);
        totalGems++;
        Gem gem2 = PropFactory.createGem(grid, grid.makeGridPosition(12, 1));
        lvlObjs.add(gem2);
        lvlGems.add(gem2);
        totalGems++;

        portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(1, 10));
        lvlObjs.add(portal);


        ArrayList<Enemy> lvlEnemies = new ArrayList<>();

        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(1, 4), GridDirection.DOWN, controller.getPlayer());
        lvlObjs.add(a);
        lvlEnemies.add(a);

        enemies = new Beetle[lvlEnemies.size()];
        lvlEnemies.toArray(enemies);

        gameObjs = new GameObject[lvlObjs.size()]; //Pedir ao João para me explicar melhor isto
        lvlObjs.toArray(gameObjs);

        gems = new Gem[lvlGems.size()];
        lvlGems.toArray(gems);

        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.LEFT);
        a = null;
    }

    public void countAndBuildGameObjectsLvl5() {

        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        gameObjs = null;
        enemies = null;

        ArrayList<GameObject> lvl5Objs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();

        //Cycle to build side walls
        for (int i = 0; i < grid.getRows(); i++) {
            lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }

        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            if (i == grid.getCols() - 17) {
                continue;
            }
            lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }

        //row 1
        for (int i = 4; i < grid.getCols() - 3; i++) {
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 5) {
                continue;
            }
            lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 1)));
        }
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(13, 1)));

        //row 2
        for (int i = 4; i < grid.getCols() - 5; i++) {
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 2)));
        }
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(13, 2)));
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(14, 2)));
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(3, 2)));
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(9, 2)));
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(11, 2)));

        //row 3
        for (int i = 4; i < grid.getCols() - 3; i++) {
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 3)));
        }
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(3, 3)));
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(11, 3)));
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(9, 3)));

        //row 4
        for (int i = 3; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 10) {
                continue;
            }
            lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 4)));
        }

        //row 5
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(3, 5)));
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(8, 5)));

        //row 6
        for (int i = 1; i < grid.getCols() - 3; i++) {
            if (i == grid.getCols() - 16) {
                lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(2, 6)));
                continue;
            }
            if (i == grid.getCols() - 15) {
                lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(3, 6)));
                continue;
            }
            if (i == grid.getCols() - 11) {
                lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(7, 6)));
                continue;
            }
            if (i == grid.getCols() - 10) {
                lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 6)));
                continue;
            }
            if (i == grid.getCols() - 9) {
                lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(9, 6)));
                continue;
            }
            lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 6)));
        }

        //row 7
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(2, 7)));
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(3, 7)));
        lvl5Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(13, 7)));
        lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(9, 7)));
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(10, 7)));

        //row 8
        for (int i = 7; i < grid.getCols() - 3; i++) {
            if (i == grid.getCols() - 8) {
                lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(10, 8)));
                continue;
            }
            if(i==grid.getCols()-7){
                continue;
            }
            lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 8)));
        }

        //row 9
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(6, 9)));
        for (int i = 7; i < grid.getCols() - 3; i++) {
            if (i == grid.getCols() - 8) {
                lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(10, 9)));
                continue;
            }
            if (i == grid.getCols() - 7) {
                lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(11, 9)));
                continue;
            }
            lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 9)));
        }
        lvl5Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(15, 9)));

        //row 10
        for (int i = 5; i < grid.getCols() - 1; i++) {
            lvl5Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 10)));
        }

        Gem gem1 = PropFactory.createGem(grid, grid.makeGridPosition(8, 7));
        lvl5Objs.add(gem1);
        lvlGems.add(gem1);
        totalGems++;

        portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(1, 11));
        lvl5Objs.add(portal);



        ArrayList<Enemy> lvlEnemies = new ArrayList<>();

        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(1, 1), GridDirection.RIGHT, controller.getPlayer());
        lvl5Objs.add(a);
        lvlEnemies.add(a);

        enemies = new Beetle[lvlEnemies.size()];
        lvlEnemies.toArray(enemies);

        gameObjs = new GameObject[lvl5Objs.size()]; //Pedir ao João para me explicar melhor isto
        lvl5Objs.toArray(gameObjs);

        gems = new Gem[lvlGems.size()];
        lvlGems.toArray(gems);

        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.UP);
        a = null;
    }

    public void countAndBuildGameObjectsLvl6() {

        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        gameObjs = null;
        enemies = null;

        ArrayList<GameObject> lvl1Objs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();

        //Cycle to build side walls
        for (int i = 0; i < grid.getRows(); i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }
        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            if (i == grid.getCols() - 17) {
                portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(i, lowestCell));
                lvl1Objs.add(portal);
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }

        // row one

        //row two
        for (int i = 3; i < grid.getCols() - 11; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 2)));
        }

        //row three
        for (int i = 10; i < grid.getCols() - 2; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 3)));
        }

        //row four
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(15, 4)));

        //row five
        for (int i = 12; i < grid.getCols() - 3; i++) {
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 5)));
        }
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(10, 5)));
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(11, 5)));

        //row six
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(7, 6)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(8, 6)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(9, 6)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(13, 6)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(14, 6)));

        //row seven
        for (int i = 5; i < grid.getCols() - 4; i++) {
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 11) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 7)));
        }
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(6, 7)));
        lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(14, 7)));

        // row eight
        for (int i = 10; i < grid.getCols() - 3; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 8)));
        }

        //row nine
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(4, 9)));
        lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(5, 9)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(3, 9)));
        lvl1Objs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(6, 9)));

        //row ten

        //column 2
        for (int i = 2; i < grid.getRows() - 2; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(2, i)));
        }

        //column 7
        for (int i = 1; i < grid.getRows() - 2; i++) {
            if (i == grid.getRows() - 6) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(7, i)));
        }

        //column 8
        for (int i = 2; i < grid.getRows() - 6; i++) {
            lvl1Objs.add(PropFactory.createRock(grid, grid.makeGridPosition(8, i)));
        }

        //column 9
        for (int i = 3; i < grid.getRows() - 1; i++) {
            if (i == grid.getRows() - 6) {
                continue;
            }
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(9, i)));
        }
        //column 15
        for (int i = 5; i < grid.getRows() - 3; i++) {
            lvl1Objs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(15, i)));
        }


        //Build props, gems
        Gem gem1 = PropFactory.createGem(grid, grid.makeGridPosition(14, 4));
        lvl1Objs.add(gem1);
        lvlGems.add(gem1);
        totalGems++;

        Gem gem2 = PropFactory.createGem(grid, grid.makeGridPosition(8, 1));
        lvl1Objs.add(gem2);
        lvlGems.add(gem2);
        totalGems++;

        portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(1, 11));
        lvl1Objs.add(portal);



        ArrayList<Enemy> lvlEnemies = new ArrayList<>();

        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(3, 3), GridDirection.RIGHT, controller.getPlayer());
        lvl1Objs.add(a);
        lvlEnemies.add(a);


        gameObjs = new GameObject[lvl1Objs.size()];
        gems = new Gem[lvlGems.size()];
        lvl1Objs.toArray(gameObjs);
        lvlGems.toArray(gems);
        enemies = new Beetle[lvlEnemies.size()];
        lvlEnemies.toArray(enemies);

        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.UP);
        a = null;


    }

    public void countAndBuildGameObjectsLvl7() {
        controller.getPlayer().setPos(grid.makeGridPosition(16, 1));

        gameObjs = null;
        enemies = null;

        ArrayList<GameObject> lvlObjs = new ArrayList<>();
        ArrayList<Gem> lvlGems = new ArrayList<>();

        //Cycle to build ceiling and floor except for the corner squares built by the side walls
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 0)));
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, lowestCell)));
        }
        for (int i = 0; i < grid.getRows(); i++) {
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(0, i)));
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(grid.getCols() - 1, i)));
        }
        //one more layer of walls at the bottom
        for (int i = 2; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 15) {
                continue;
            }
            if (i == grid.getCols() - 13) {
                continue;
            }
            if (i == grid.getCols() - 11) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            if (i == grid.getCols() - 5) {
                continue;
            }
            if (i == grid.getCols() - 3) {
                continue;
            }
            if (i == grid.getCols() - 1) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 10)));
        }

        //build all the grass
        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 14) {
                continue;
            }
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            if (i == grid.getCols() - 2) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 1)));
        }
        for (int i = 1; i < grid.getCols() - 1; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 2)));
        }
        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 14) {
                continue;
            }
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 3)));
        }
        for (int i = 1; i < grid.getCols() - 14; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 4)));
        }
        lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(16, 4)));

        for (int i = 1; i < grid.getCols() - 14; i++) {
            lvlObjs.add(TerrainFactory.createSand(grid, grid.makeGridPosition(i, 5)));
        }

        for (int i = 5; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            if (i == grid.getCols() - 2) {
                continue;
            }
            lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 4)));
        }
        for (int i = 5; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 5)));
        }

        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 14) {
                continue;
            }
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 6)));
        }
        for (int i = 1; i < grid.getCols() - 1; i++) {
            if (i == grid.getCols() - 14) {
                continue;
            }
            if (i == grid.getCols() - 12) {
                continue;
            }
            if (i == grid.getCols() - 10) {
                continue;
            }
            if (i == grid.getCols() - 8) {
                continue;
            }
            if (i == grid.getCols() - 6) {
                continue;
            }
            if (i == grid.getCols() - 4) {
                continue;
            }
            lvlObjs.add(TerrainFactory.createWall(grid, grid.makeGridPosition(i, 7)));
        }

        for (int i = 4; i < grid.getCols() - 2; i++) {
            if (i == grid.getCols() - 13) {
                continue;
            }
            if (i == grid.getCols() - 11) {
                continue;
            }
            if (i == grid.getCols() - 9) {
                continue;
            }
            if (i == grid.getCols() - 7) {
                continue;
            }
            if (i == grid.getCols() - 5) {
                continue;
            }
            if (i == grid.getCols() - 3) {
                continue;
            }
            lvlObjs.add(PropFactory.createRock(grid, grid.makeGridPosition(i, 1)));
        }


        Gem gem1 = PropFactory.createGem(grid, grid.makeGridPosition(14, 3));
        lvlObjs.add(gem1);
        lvlGems.add(gem1);
        totalGems++;

        Gem gem2 = PropFactory.createGem(grid, grid.makeGridPosition(12, 3));
        lvlObjs.add(gem2);
        lvlGems.add(gem2);
        totalGems++;

        Gem gem3 = PropFactory.createGem(grid, grid.makeGridPosition(10, 3));
        lvlObjs.add(gem3);
        lvlGems.add(gem3);
        totalGems++;

        Gem gem4 = PropFactory.createGem(grid, grid.makeGridPosition(8, 3));
        lvlObjs.add(gem4);
        lvlGems.add(gem4);
        totalGems++;

        Gem gem5 = PropFactory.createGem(grid, grid.makeGridPosition(6, 3));
        lvlObjs.add(gem5);
        lvlGems.add(gem5);
        totalGems++;

        Gem gem6 = PropFactory.createGem(grid, grid.makeGridPosition(4, 3));
        lvlObjs.add(gem6);
        lvlGems.add(gem6);
        totalGems++;

        portal = TerrainFactory.createPortal(grid, grid.makeGridPosition(1, 10));
        lvlObjs.add(portal);


        ArrayList<Enemy> lvlEnemies = new ArrayList<>();
        Beetle a = EnemyFactory.createBeetle(grid, grid.makeGridPosition(1, 8), GridDirection.RIGHT, controller.getPlayer());
        lvlObjs.add(a);
        lvlEnemies.add(a);

        gameObjs = new GameObject[lvlObjs.size()]; //Pedir ao João para me explicar melhor isto
        lvlObjs.toArray(gameObjs);

        gems = new Gem[lvlGems.size()];
        lvlGems.toArray(gems);

        enemies = new Beetle[lvlEnemies.size()];
        lvlEnemies.toArray(enemies);

        a.setObjectsToInteract(gameObjs);
        a.setWallDirection(GridDirection.UP);
        a = null;

    }

    public void clearCanvas() {

        ArrayList<Shape> shapes = Canvas.getInstance().getShapes();
        Shape[] shapess = new Shape[shapes.size()];
        shapess = Canvas.getInstance().getShapes().toArray(shapess);
        for (Shape shape : shapess) {
            shape.delete();
        }
    }

    public void gameBehavior() throws InterruptedException {

        while (!controller.getPlayer().isInPortal()) {

            //Pause for a while
            Thread.sleep(delay);

            gravity();

            portalBehavior();

            if (enemies != null) {
                for (Beetle enemy : enemies) {
                    enemy.move(enemy.getDirection());
                }
            }

        }

        controller.getPlayer().setInPortalFalse();
        resetGems();
        controller.deactivate();
        hold();
        clearCanvas();
    }

    public void resetGems() {
        gemCounter = 0;
        totalGems = 0;
    }

    public Grid getGrid() {
        return grid;
    }

    public void hold() {
        isHolding = true;
    }

    public void liberate() {
        isHolding = false;
    }

    //restart level
    public void resetLevel() {
        if (grid instanceof SimpleGfxGrid) {
            int cell = ((SimpleGfxGrid) grid).getCellSize();

            for (GameObject obj : gameObjs) {
                Picture objPicture;
                if (obj.getPos() instanceof SimpleGfxGridPosition) {
                    objPicture = ((SimpleGfxGridPosition) obj.getPos()).getPicture();
                    objPicture.translate(((obj.getPosI().getCol() - obj.getPos().getCol()) * cell), ((obj.getPosI().getRow() - obj.getPos().getRow()) * cell));
                }
                obj.getPos().setPos(obj.getPosI().getCol(), obj.getPosI().getRow());
                if (obj instanceof Destroyable) {
                    ((Destroyable) obj).recreate();
                }
            }
            Picture pPic;
            if (controller.getPlayer().getPos() instanceof SimpleGfxGridPosition) {
                pPic = ((SimpleGfxGridPosition) controller.getPlayer().getPos()).getPicture();
                pPic.translate(((controller.getPlayer().getPosI().getCol() - controller.getPlayer().getPos().getCol()) * cell), ((controller.getPlayer().getPosI().getRow() - controller.getPlayer().getPos().getRow()) * cell));
            }
            controller.getPlayer().getPos().setPos(controller.getPlayer().getPosI().getCol(), controller.getPlayer().getPosI().getRow());
        }
        gemCounter = 0;
        if (portal.isOpen()) {
            portal.close();
        }
        if (!controller.getPlayer().exists()) {
            controller.getPlayer().revive();
        }
    }


}
