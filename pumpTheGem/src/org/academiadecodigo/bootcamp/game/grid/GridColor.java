package org.academiadecodigo.bootcamp.game.grid;

public enum GridColor {
    RED,
    GREEN,
    BLUE,
    MAGENTA,
    BLACK,
    CYAN,
    DARK_GRAY,
    GRAY,
    LIGHT_GRAY,
    ORANGE,
    PINK,
    YELLOW,
    NOCOLOR;

}
