package org.academiadecodigo.bootcamp.game;

import org.academiadecodigo.bootcamp.game.gfx.simplegfx.SimpleGfxGrid;

public class Main {


    public static void main(String[] args) throws InterruptedException {

        SimpleGfxGrid grid = new SimpleGfxGrid(18,12);
        Game game = new Game(grid, 300);
        while(true) {
            game.menu();
        }

    }


}






