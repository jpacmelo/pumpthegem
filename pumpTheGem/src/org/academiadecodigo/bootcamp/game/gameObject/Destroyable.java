package org.academiadecodigo.bootcamp.game.gameObject;

public interface Destroyable {

    public abstract void destroy();

    public abstract boolean exists();

    public abstract void recreate();
}
