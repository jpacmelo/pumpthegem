package org.academiadecodigo.bootcamp.game.gameObject.terrain;

import org.academiadecodigo.bootcamp.game.gameObject.Teleportable;
import org.academiadecodigo.bootcamp.game.gameObject.characters.Player;
import org.academiadecodigo.bootcamp.game.gameObject.props.Gem;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridColor;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class Portal extends Terrain{

    private GridPosition posUp;
    //private int gems=0;
    private boolean isOpen = false;

    public Portal(Grid grid, GridPosition pos){
        super(grid, pos, TerrainType.PORTAL);
        posUp = grid.makeGridPosition(pos.getCol(),pos.getRow()-1);
        posUp.setColor(GridColor.NOCOLOR);
        posUp.hide();
    }

    public void open(){
        isOpen = true;
    }

    public void close(){
        isOpen = false;
    }

    public boolean isOpen() {
        return isOpen;
    }

    /*public int getGems(){
        return gems;
    }*/

    public boolean getTeleportable(Teleportable teleportable){
        if(teleportable instanceof Player){
            Player player = (Player) teleportable;
            if (player.getPos().equals(posUp)) {
                player.moveToPortal();
                return true;
            }
        } else if(teleportable instanceof Gem){
            Gem gem = (Gem) teleportable;
            if(gem.getPos().equals(posUp)&&gem.exists()) {
                gem.moveToPortal();
                //gems++;
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "Portal";
    }
}
