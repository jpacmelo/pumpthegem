package org.academiadecodigo.bootcamp.game.gameObject.terrain;

import org.academiadecodigo.bootcamp.game.gameObject.Destroyable;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class Sand extends Terrain implements Destroyable {
    private boolean exists = true;

    public Sand(Grid grid, GridPosition pos){
        super(grid, pos, TerrainType.SAND);

    }

    @Override
    public void destroy(){
        exists = false;
        getPos().hide();
    }
    @Override
    public boolean exists(){
        return exists;
    }

    @Override
    public void recreate(){
        exists=true;
    }

    @Override
    public String toString(){
        return "Sand";
    }


}
