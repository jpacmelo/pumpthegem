package org.academiadecodigo.bootcamp.game.gameObject.terrain;

import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public abstract class Terrain extends GameObject {

    private TerrainType terrainType;

    public Terrain(Grid grid, GridPosition pos, TerrainType terrainType) {
        super(grid,pos);
        this.terrainType = terrainType;
        if(this.terrainType.getImgName()!="") {
            pos.setImg(terrainType.getImgName());
        } else {
            pos.setColor(terrainType.getColor());
        }
    }

    @Override
    public String toString(){
        return "Terrain";
    }
}
