package org.academiadecodigo.bootcamp.game.gameObject.terrain;

import org.academiadecodigo.bootcamp.game.grid.GridColor;

public enum TerrainType {
    WALL(GridColor.PINK,"wall.png"),
    SAND(GridColor.YELLOW, "sand.png"),
    PORTAL(GridColor.MAGENTA, "portal.png");

    private GridColor color;
    private String imgName;

    TerrainType(GridColor color, String imgName){
        this.color=color;
        this.imgName=imgName;
    }

    public GridColor getColor() {
        return color;
    }

    public String getImgName(){
        return imgName;
    }
}
