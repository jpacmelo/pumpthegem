package org.academiadecodigo.bootcamp.game.gameObject.terrain;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class Wall extends Terrain{

    public Wall(Grid grid, GridPosition pos){
        super(grid,pos,TerrainType.WALL);
    }

    @Override
    public String toString(){
        return "Wall";
    }
}
