package org.academiadecodigo.bootcamp.game.gameObject.terrain;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class TerrainFactory {

    public static Wall createWall(Grid grid, GridPosition pos) {
        return new Wall(grid, pos);
    }

    public static Sand createSand(Grid grid, GridPosition pos) {
        return new Sand(grid, pos);
    }

    public static Portal createPortal(Grid grid, GridPosition pos){
        return new Portal(grid, pos);
    }

}
