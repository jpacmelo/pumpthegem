package org.academiadecodigo.bootcamp.game.gameObject;

import org.academiadecodigo.bootcamp.game.grid.GridDirection;

public interface Movable {

    public abstract void move(GridDirection gridDirection);
}
