package org.academiadecodigo.bootcamp.game.gameObject;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public abstract class GameObject {
    private GridPosition pos;
    private GridPosition posI;
    private Grid grid;

    public GameObject(Grid grid, GridPosition pos){
        this.pos = pos;
        this.grid = grid;
        this.posI = this.grid.makeGridPosition(pos.getCol(),pos.getRow());
    }

    public GridPosition getPos() {
        return pos;
    }

    public void setPos(GridPosition pos) {
        this.pos = pos;
        pos.show();
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    @Override
    public String toString(){
        return "Game Object";
    }

    public GridPosition getPosI() {
        return posI;
    }
}
