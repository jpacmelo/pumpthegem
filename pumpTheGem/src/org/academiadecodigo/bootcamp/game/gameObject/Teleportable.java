package org.academiadecodigo.bootcamp.game.gameObject;

public interface Teleportable {

    public abstract void moveToPortal();

}
