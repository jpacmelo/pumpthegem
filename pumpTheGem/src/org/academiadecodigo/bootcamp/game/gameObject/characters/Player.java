package org.academiadecodigo.bootcamp.game.gameObject.characters;

import org.academiadecodigo.bootcamp.game.gameObject.Destroyable;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.gameObject.Movable;
import org.academiadecodigo.bootcamp.game.gameObject.Teleportable;
import org.academiadecodigo.bootcamp.game.gameObject.props.Prop;
import org.academiadecodigo.bootcamp.game.gameObject.terrain.Sand;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridColor;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;


public class Player implements Movable, Destroyable, Teleportable {

    private String name;
    private boolean isDead=false;
    private Grid grid;
    private GridPosition pos;
    private GridPosition posI;
    private boolean inPortal = false;
    private GridColor color = GridColor.CYAN;
    private GameObject[] objectsToInteract;
    private String imgName = "player.png";

    public Player(String name, Grid grid/*, GridPosition pos*/){
        this.name = name;
        this.grid = grid;
        //this.pos = pos;
        //pos.setColor(color);
        //pos.setImg(imgName);
    }

    public String getName(){
        return name;
    }

    @Override
    public void destroy(){
        die();
    }

    public void die(){
        isDead = true;
        pos.hide();
    }

    public void revive(){
        isDead=false;
        pos.show();
    }

    public void destroySand(/*GridPosition position, gameObject, */){

    }

    public void setPos(GridPosition pos) {
        this.pos = pos;
        this.pos.setImg(imgName);
        this.posI = this.grid.makeGridPosition(pos.getCol(),pos.getRow());
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public GridPosition getPosI() {
        return posI;
    }

    public void move(GridDirection gridDirection){
        if(!isDead){
            boolean blocked = false;
            for (GameObject obj: objectsToInteract) {
                switch (gridDirection) {
                    case UP:
                        //Check if position directly up my player has any object, if so, try to interact with the object
                        if(obj.getPos().getCol() == getPos().getCol() && obj.getPos().getRow() == getPos().getRow()-1){
                            blocked = interact(obj,gridDirection);
                        }
                        break;
                    case DOWN:
                        //Check if position directly bellow my player has any object, if so, try to interact with the object
                        if(obj.getPos().getCol() == getPos().getCol() && obj.getPos().getRow() == getPos().getRow()+1){
                            blocked = interact(obj,gridDirection);
                        }
                        break;
                    case LEFT:
                        //Check if position directly left to my player has any object, if so, try to interact with the object
                        if(obj.getPos().getCol() == getPos().getCol()-1 && obj.getPos().getRow() == getPos().getRow()){
                           blocked = interact(obj,gridDirection);
                        }
                        break;
                    case RIGHT:
                        //Check if position directly right to my player has any object, if so, try to interact with the object
                        if(obj.getPos().getCol() == getPos().getCol()+1 && obj.getPos().getRow() == getPos().getRow()){
                            blocked = interact(obj,gridDirection);
                        }
                        break;
                }
                if(blocked){
                    break;
                }
            }
            if(!blocked) {
                pos.moveInDirection(gridDirection, 1);
            }
        }
    }

    public void moveToPortal(){
        inPortal = true;
    }

    public boolean isInPortal(){
        return inPortal;
    }

    public void setInPortalFalse(){
        inPortal=false;
    }

    public GridPosition getPos() {
        return pos;
    }

    //push a Prop
    public boolean push(GameObject obj, GridDirection gridDirection){
        boolean empty = true;
        switch(gridDirection){
            case LEFT:
                //check if there is an object next to my prop
                for (GameObject otherObj:objectsToInteract){
                    // do not compare with itself
                    if(otherObj != obj){
                        //if position matches
                        if(obj.getPos().getCol()-1 == otherObj.getPos().getCol() && obj.getPos().getRow()==otherObj.getPos().getRow()){
                            //and obj is destroyable
                            if(otherObj instanceof Destroyable){
                                if(((Destroyable) otherObj).exists()){
                                    empty = false;
                                }
                            } else {
                                empty = false;
                            }
                        }
                    }
                }
                break;
            case RIGHT:
                //check if there is an object next to my prop
                for (GameObject otherObj:objectsToInteract){
                    // do not compare with itself
                    if(otherObj != obj){
                        //if position matches
                        if(obj.getPos().getCol()+1 == otherObj.getPos().getCol() && obj.getPos().getRow() == otherObj.getPos().getRow()){
                            //and obj is destroyable
                            if(otherObj instanceof Destroyable){
                                //if the destroyable object exists
                                if(((Destroyable) otherObj).exists()){
                                    empty = false;
                                }
                            } else {
                                empty = false;
                            }
                        }
                    }
                }
                break;
            default:
                return true;

        }

        if(empty){
            ((Prop) obj).move(gridDirection);
            return false;
        }
        return true;
    }

    public GridColor getColor() {
        return color;
    }

    public void setObjectsToInteract(GameObject[] gameObjects){
        objectsToInteract = gameObjects;
    }

    public boolean exists(){
        return !isDead;
    }

    @Override
    public void recreate() {
        isDead=false;
    }

    public boolean interact(GameObject obj, GridDirection gridDirection){
        // Interact will return blocked, if it is not possible to interact with the object, the player is blocked
        boolean blocked = false;
        if(obj instanceof Sand){
            if(((Sand) obj).exists()) {
                ((Sand) obj).destroy();
                blocked = false;
            }
        } else if(obj instanceof Prop) {
            if (((Prop) obj).exists()){
                blocked = push(obj, gridDirection);
            }
        } else {
            blocked = true;
        }
        return blocked;

    }
}
