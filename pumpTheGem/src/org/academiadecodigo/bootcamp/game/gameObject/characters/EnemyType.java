package org.academiadecodigo.bootcamp.game.gameObject.characters;

import org.academiadecodigo.bootcamp.game.grid.GridColor;

public enum EnemyType {
    BEETLE(GridColor.GREEN, "beetle.png");

    private GridColor color;
    private String imgName;

    EnemyType(GridColor color, String imgName) {
        this.color = color;
        this.imgName = imgName;
    }

    public GridColor getColor() {
        return color;
    }

    public String getImgName() {
        return imgName;
    }
}
