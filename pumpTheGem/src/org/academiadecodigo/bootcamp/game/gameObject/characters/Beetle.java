package org.academiadecodigo.bootcamp.game.gameObject.characters;

import org.academiadecodigo.bootcamp.game.gameObject.Destroyable;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class Beetle extends Enemy{

    private GridDirection dir;
    private GameObject[] gameObjects;
    private GridDirection wallDirection;
    private Player player;

    public Beetle(Grid grid, GridPosition pos, GridDirection gridDir, Player player){
        super(grid, pos, EnemyType.BEETLE, gridDir);
        this.dir=gridDir;
        this.player = player;
    }

    public void setObjectsToInteract(GameObject[] gameObjects){
        this.gameObjects = gameObjects;
    }
    public void setWallDirection(GridDirection gridDirection){
        wallDirection = gridDirection;
    }
    public void setDir(GridDirection gridDirection){
        dir=gridDirection;
    }

    public GridDirection getWallDirection() {
        return wallDirection;
    }

    @Override
    public void move(GridDirection direction){
        boolean leftEmpty=true;
        boolean rightEmpty=true;
        boolean upEmpty=true;
        boolean downEmpty=true;
        for (GameObject obj : gameObjects){
            if(obj.getPos().getRow()==getPos().getRow()-1 && obj.getPos().getCol()==getPos().getCol()){
                if(obj instanceof Destroyable){
                    if(!((Destroyable) obj).exists()){
                        continue;
                    }
                }
                upEmpty = false;
            }
            if(obj.getPos().getRow()==getPos().getRow()+1 && obj.getPos().getCol()==getPos().getCol()){
                if(obj instanceof Destroyable){
                    if(!((Destroyable) obj).exists()){
                        continue;
                    }
                }
                downEmpty = false;
            }
            if(obj.getPos().getRow()==getPos().getRow() && obj.getPos().getCol()==getPos().getCol()-1){
                if(obj instanceof Destroyable){
                    if(!((Destroyable) obj).exists()){
                        continue;
                    }
                }
                leftEmpty = false;
            }
            if(obj.getPos().getRow()==getPos().getRow() && obj.getPos().getCol()==getPos().getCol()+1){
                if(obj instanceof Destroyable){
                    if(!((Destroyable) obj).exists()){
                        continue;
                    }
                }
                rightEmpty = false;
            }
        }
        if(getPos().getRow()==(getGrid().getRows()-1)){
            downEmpty=false;
        }
        if(getPos().getRow()==0){
            upEmpty=false;
        }
        if(getPos().getCol()==0){
            leftEmpty=false;
        }
        if (getPos().getCol()==(getGrid().getCols()-1)){
            rightEmpty=false;
        }
        if(wallDirection.equals(GridDirection.UP)) {
            if (upEmpty) {
                getPos().moveInDirection(wallDirection, 1);
                if(player.getPos().equals(getPos())){
                    killPlayer(player);
                }
                if (dir.equals(GridDirection.LEFT)) {
                    wallDirection = GridDirection.RIGHT;
                }
                if (dir.equals(GridDirection.RIGHT)) {
                    wallDirection = GridDirection.LEFT;
                }
                dir = GridDirection.UP;
                return;
            }
        }
        if(wallDirection.equals(GridDirection.DOWN)) {
            if (downEmpty) {
                getPos().moveInDirection(wallDirection, 1);
                if(player.getPos().equals(getPos())){
                    killPlayer(player);
                }
                if (dir.equals(GridDirection.LEFT)) {
                    wallDirection = GridDirection.RIGHT;
                }
                if (dir.equals(GridDirection.RIGHT)) {
                    wallDirection = GridDirection.LEFT;
                }
                dir = GridDirection.DOWN;
                return;
            }
        }
        if(wallDirection.equals(GridDirection.RIGHT)) {
            if (rightEmpty) {
                getPos().moveInDirection(wallDirection, 1);
                if(player.getPos().equals(getPos())){
                    killPlayer(player);
                }
                if (dir.equals(GridDirection.UP)) {
                    wallDirection = GridDirection.DOWN;
                }
                if (dir.equals(GridDirection.DOWN)) {
                    wallDirection = GridDirection.UP;
                }
                dir = GridDirection.RIGHT;
                return;
            }
        }
        if(wallDirection.equals(GridDirection.LEFT)) {
            if (leftEmpty) {
                getPos().moveInDirection(wallDirection, 1);
                if(player.getPos().equals(getPos())){
                    killPlayer(player);
                }
                if (dir.equals(GridDirection.UP)) {
                    wallDirection = GridDirection.DOWN;
                }
                if (dir.equals(GridDirection.DOWN)) {
                    wallDirection = GridDirection.UP;
                }
                dir = GridDirection.LEFT;
                return;
            }
        }
        switch (dir){

            case UP:
                if(upEmpty){
                    getPos().moveInDirection(dir,1);
                    if(player.getPos().equals(getPos())){
                        killPlayer(player);
                    }
                    return;
                } else {
                    if (!leftEmpty && !rightEmpty) {
                        dir = GridDirection.DOWN;
                        getPos().moveInDirection(dir, 1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        if (wallDirection.equals(GridDirection.LEFT)) {
                            wallDirection = GridDirection.RIGHT;
                        } else if (wallDirection.equals(GridDirection.RIGHT)) {
                            wallDirection = GridDirection.LEFT;
                        }
                        return;
                    } else if (wallDirection.equals(GridDirection.RIGHT)) {
                        dir = GridDirection.LEFT;
                        getPos().moveInDirection(dir, 1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.UP;
                        return;
                    } else if (wallDirection.equals(GridDirection.LEFT)) {
                        dir = GridDirection.RIGHT;
                        getPos().moveInDirection(dir, 1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.UP;
                        return;
                    }
                }
                return;
            case DOWN:
                if(downEmpty){
                    getPos().moveInDirection(dir,1);
                    if(player.getPos().equals(getPos())){
                        killPlayer(player);
                    }
                    return;
                } else{
                    if(!leftEmpty && !rightEmpty){
                        dir = GridDirection.UP;
                        getPos().moveInDirection(dir,1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        if(wallDirection.equals(GridDirection.LEFT)){
                            wallDirection = GridDirection.RIGHT;
                        }else if(wallDirection.equals(GridDirection.RIGHT)){
                            wallDirection = GridDirection.LEFT;
                        }
                        return;
                    } else if(wallDirection.equals(GridDirection.RIGHT)){
                        dir = GridDirection.LEFT;
                        getPos().moveInDirection(dir,1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.DOWN;
                        return;
                    } else if(wallDirection.equals(GridDirection.LEFT)){
                        dir = GridDirection.RIGHT;
                        getPos().moveInDirection(dir,1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.DOWN;
                        return;
                    }
                }
                break;
            case LEFT:
                if(leftEmpty){
                    getPos().moveInDirection(dir,1);
                    if(player.getPos().equals(getPos())){
                        killPlayer(player);
                    }
                    return;
                } else{
                    if(!upEmpty && !downEmpty){
                        dir = GridDirection.RIGHT;
                        if(wallDirection.equals(GridDirection.UP)){
                            wallDirection = GridDirection.DOWN;
                        } else if(wallDirection.equals(GridDirection.DOWN)){
                            wallDirection = GridDirection.UP;
                        }
                        getPos().moveInDirection(dir,1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        return;
                    } else if(wallDirection.equals(GridDirection.UP)){
                        dir = GridDirection.DOWN;
                        getPos().moveInDirection(dir,1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.LEFT;
                        return;
                    } else if(wallDirection.equals(GridDirection.DOWN)){
                        dir = GridDirection.UP;
                        getPos().moveInDirection(dir,1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.LEFT;
                        return;
                    }
                }
                return;
            case RIGHT:
                if(rightEmpty){
                    getPos().moveInDirection(dir,1);
                    if(player.getPos().equals(getPos())){
                        killPlayer(player);
                    }
                    return;
                } else {
                    if (!upEmpty && !downEmpty) {
                        dir = GridDirection.LEFT;
                        getPos().moveInDirection(dir, 1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        if (wallDirection.equals(GridDirection.UP)) {
                            wallDirection = GridDirection.DOWN;
                        } else if (wallDirection.equals(GridDirection.DOWN)) {
                            wallDirection = GridDirection.UP;
                        }
                        return;
                    } else if (wallDirection.equals(GridDirection.UP)) {
                        dir = GridDirection.DOWN;
                        getPos().moveInDirection(dir, 1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.RIGHT;
                        return;
                    } else if (wallDirection.equals(GridDirection.DOWN)) {
                        dir = GridDirection.UP;
                        getPos().moveInDirection(dir, 1);
                        if(player.getPos().equals(getPos())){
                            killPlayer(player);
                        }
                        wallDirection = GridDirection.RIGHT;
                        return;
                    }
                    return;
                }

        }

    }

    @Override
    public String toString() {
        return "Beetle";
    }

}
