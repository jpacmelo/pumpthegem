package org.academiadecodigo.bootcamp.game.gameObject.characters;

import org.academiadecodigo.bootcamp.game.gameObject.Destroyable;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.gameObject.Movable;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public abstract class Enemy extends GameObject implements Movable, Destroyable {

    private boolean isDead=false;
    private GridDirection dir;
    private EnemyType enemyType;

    public Enemy(Grid grid, GridPosition pos, EnemyType enemyType, GridDirection gridDir) {
        super(grid, pos);
        this.enemyType = enemyType;
        pos.setImg(enemyType.getImgName());
        dir = gridDir;
    }

    public void die() {
        isDead = true;
    }

    public void killPlayer(Player player) {
        player.destroy();
    }

    public void move(GridDirection direction) {
        return;
    }

    public GridDirection getDirection() {
        return dir;
    }

    public void destroy() {
        die();
    }

    public boolean exists(){
        return !isDead;
    }

    @Override
    public void recreate() {
        isDead=false;
    }

    public Grid getGrid(){
        return super.getGrid();
    }
}
