package org.academiadecodigo.bootcamp.game.gameObject.characters;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class EnemyFactory {

    public static Beetle createBeetle(Grid grid, GridPosition pos, GridDirection gridDir, Player player){
        return new Beetle(grid, pos, gridDir, player);
    }

}
