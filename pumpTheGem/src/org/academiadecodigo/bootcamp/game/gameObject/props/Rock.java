package org.academiadecodigo.bootcamp.game.gameObject.props;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class Rock extends Prop {

    public Rock(Grid grid, GridPosition pos) {
        super(grid, pos, PropType.ROCK);
    }

    @Override
    public String toString(){
        return "Rock";
    }
}
