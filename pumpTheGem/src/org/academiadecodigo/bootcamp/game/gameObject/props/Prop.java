package org.academiadecodigo.bootcamp.game.gameObject.props;

import org.academiadecodigo.bootcamp.game.gameObject.Destroyable;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.gameObject.Movable;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public abstract class Prop extends GameObject implements Movable, Destroyable {

    private boolean exists = true;
    private boolean falling = false;
    private PropType propType;

    public Prop(Grid grid, GridPosition pos, PropType propType) {
        super(grid,pos);
        this.propType = propType;
        if(this.propType.getImgName()!="") {
            pos.setImg(propType.getImgName());
        }else {
            pos.setColor(propType.getColor());
        }
    }


    @Override
    public boolean exists() {
        return exists;
    }

    public boolean isFalling() {
        return falling;
    }

    public void fall() {
        falling = true;
    }

    public void land(){
        falling = false;
    }

    @Override
    public void move(GridDirection direction) {
        getPos().moveInDirection(direction,1);
    }

    @Override
    public void destroy() {
        exists = false;
        getPos().hide();
    }

    @Override
    public void recreate(){
        exists = true;
    }

    @Override
    public String toString(){
        return "Prop Class";
    }

}
