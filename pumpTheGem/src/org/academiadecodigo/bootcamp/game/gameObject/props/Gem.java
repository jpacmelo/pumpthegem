package org.academiadecodigo.bootcamp.game.gameObject.props;

import org.academiadecodigo.bootcamp.game.gameObject.Teleportable;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class Gem extends Prop implements Teleportable {

    private boolean inPortal = false;

    public Gem(Grid grid, GridPosition pos) {
        super(grid, pos, PropType.GEM);
    }

    public void moveToPortal() {
        inPortal = true;
        destroy();
    }

    @Override
    public String toString() {
        return "Gem";
    }

}
