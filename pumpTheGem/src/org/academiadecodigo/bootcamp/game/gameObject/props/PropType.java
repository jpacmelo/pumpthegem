package org.academiadecodigo.bootcamp.game.gameObject.props;

import org.academiadecodigo.bootcamp.game.grid.GridColor;

public enum PropType {
    ROCK(GridColor.DARK_GRAY, "rock.png"),
    GEM(GridColor.BLUE, "gem.png");

    private GridColor color;
    private String imgName;

    PropType(GridColor color, String imgName) {
        this.color = color;
        this.imgName = imgName;
    }

    public GridColor getColor() {
        return color;
    }

    public String getImgName() {
        return imgName;
    }
}
