package org.academiadecodigo.bootcamp.game.gameObject.props;

import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;

public class PropFactory {

    public static Rock createRock(Grid grid, GridPosition pos) {
        return new Rock(grid, pos);
    }

    public static Gem createGem(Grid grid, GridPosition pos) {
        return new Gem(grid, pos);
    }
}
