package org.academiadecodigo.bootcamp.game.controller;

import org.academiadecodigo.bootcamp.game.Game;
import org.academiadecodigo.bootcamp.game.gameObject.GameObject;
import org.academiadecodigo.bootcamp.game.gameObject.characters.Player;
import org.academiadecodigo.bootcamp.game.grid.Grid;
import org.academiadecodigo.bootcamp.game.grid.GridDirection;
import org.academiadecodigo.bootcamp.game.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Controller implements KeyboardHandler {
    private Keyboard keyboard;
    private Grid grid;
    private Player player;
    private boolean active = false;
    private Game game;
    //private CollisionDetector collisionDetector;

    public Controller(Game game, Grid grid, GridPosition gridPosition) {
        keyboard = new Keyboard(this);
        this.grid = grid;
        player = new Player("SmoothOOPerator", grid/*, gridPosition*/);
        this.game = game;
    }

    public void setObjectsToInteract(GameObject[] gameObjs) {
        player.setObjectsToInteract(gameObjs);
    }

    public void init() {

        //player.setCollisionDetector(collisionDetector);

        KeyboardEvent moveRightEvent = new KeyboardEvent();
        KeyboardEvent moveLeftEvent = new KeyboardEvent();
        KeyboardEvent moveUpEvent = new KeyboardEvent();
        KeyboardEvent moveDownEvent = new KeyboardEvent();
        KeyboardEvent moveToNextScreenEvent = new KeyboardEvent();
        KeyboardEvent resetTheCurrentLevel = new KeyboardEvent();

        moveRightEvent.setKey(KeyboardEvent.KEY_RIGHT);
        moveRightEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        moveLeftEvent.setKey(KeyboardEvent.KEY_LEFT);
        moveLeftEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        moveUpEvent.setKey(KeyboardEvent.KEY_UP);
        moveUpEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        moveDownEvent.setKey(KeyboardEvent.KEY_DOWN);
        moveDownEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        moveToNextScreenEvent.setKey(KeyboardEvent.KEY_SPACE);
        moveToNextScreenEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        resetTheCurrentLevel.setKey(KeyboardEvent.KEY_R);
        resetTheCurrentLevel.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        keyboard.addEventListener(moveRightEvent);
        keyboard.addEventListener(moveLeftEvent);
        keyboard.addEventListener(moveUpEvent);
        keyboard.addEventListener(moveDownEvent);
        keyboard.addEventListener(moveToNextScreenEvent);
        keyboard.addEventListener(resetTheCurrentLevel);

    }

    public void activate() {
        this.active = true;
    }

    public void deactivate() {
        this.active = false;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (active) {
            if (keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT) {
                player.move(GridDirection.RIGHT);
            } else if (keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT) {
                player.move(GridDirection.LEFT);
            } else if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP) {
                player.move(GridDirection.UP);
            } else if (keyboardEvent.getKey() == KeyboardEvent.KEY_DOWN) {
                player.move(GridDirection.DOWN);
            }
            if (keyboardEvent.getKey() == keyboardEvent.KEY_R) {
                game.resetLevel();
            }
        } else {
            if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
                game.liberate();
            }

        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

    public GridPosition getPos() {
        return player.getPos();
    }

    public Player getPlayer() {
        return player;
    }
}
